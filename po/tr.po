# SOME DESCRIPTIVE TITLE.
# This file is put in the public domain.
# 
# Translators:
# abc Def <hdogan1974@gmail.com>, 2020
# Bekir SONAT <bekirsonat@kde.org.tr>, 2006
msgid ""
msgstr ""
"Project-Id-Version: slapt-get\n"
"Report-Msgid-Bugs-To: woodwardj at jaos dot org\n"
"POT-Creation-Date: 2019-12-02 18:41-0500\n"
"PO-Revision-Date: 2020-03-29 21:25+0000\n"
"Last-Translator: abc Def <hdogan1974@gmail.com>\n"
"Language-Team: Turkish (http://www.transifex.com/jaos/slapt-get/language/tr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: tr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

msgid "Ready"
msgstr "Hazır"

msgid "Upgrade"
msgstr "Yükselt"

msgid "Re-Install"
msgstr "Yeniden-Kur"

msgid "<b>Required:</b>"
msgstr "<b>Gereksinim:</b>"

msgid "<b>Conflicts:</b>"
msgstr "<b>Çakışmalar:</b>"

msgid "<b>Suggests:</b>"
msgstr "<b>Öneriler:</b>"

#. installed
msgid "No dependency information available"
msgstr "No dependency information available"

msgid "Source"
msgstr "Kaynak"

msgid "No changelog information available"
msgstr "No changelog information available"

msgid "The list of installed files is only available for installed packages"
msgstr "The list of installed files is only available for installed packages"

msgid "Excluded"
msgstr "Dışlandı"

msgid "To be Removed"
msgstr "Kaldırılacak"

msgid "To be Installed"
msgstr "Kurulacak"

msgid "To be Re-Installed"
msgstr "Yeniden-kurulacak"

msgid "To be Downgraded"
msgstr "Alçaltılacak"

msgid "To be Upgraded"
msgstr "Yükseltilecek"

msgid "Installed"
msgstr "Kuruldu"

msgid "Not Installed"
msgstr "Kurulmadı"

msgid "Progress"
msgstr "Durum"

msgid "Checking for new package data..."
msgstr "Yeni paket verisi denetleniyor.."

msgid "GPG Key verification failed"
msgstr "GPG Key verification failed"

msgid "Retrieving package data..."
msgstr "Paket verileri alınıyor..."

msgid "Retrieving patch list..."
msgstr "Yama listesi alınıyor..."

msgid "Retrieving checksum list..."
msgstr "Sağlama bilgileri alınıyor..."

msgid "Retrieving checksum signature..."
msgstr "Retrieving checksum signature..."

msgid "Verifying checksum signature..."
msgstr "Verifying checksum signature..."

msgid "Retrieving ChangeLog.txt..."
msgstr "ChangeLog.txt dosyası alınıyor..."

msgid "Reading Package Lists..."
msgstr "Paket Listeleri okunuyor..."

#, c-format
msgid "Download rate: %.0f%s/s"
msgstr "İndirme hızı: %.0f%s/s"

msgid "Error"
msgstr "Error"

msgid "Completed actions"
msgstr "Tamamlanan eylemler"

msgid "Successfully executed all actions."
msgstr "Tüm eylemler başarıyla yürütüldü."

msgid "pkgtools returned an error"
msgstr "pkgtools hata bildiriyor"

msgid "Enabled"
msgstr "Etkinleştirildi"

msgid "Visible"
msgstr "Visible"

msgid "Priority"
msgstr "Priority"

msgid "Expression"
msgstr "İfade"

msgid "Packages with unmet dependencies"
msgstr "Karşılanmamış bağımlılıkları olan paketler"

msgid "Package conflicts"
msgstr "Paket çakışmaları"

msgid ", which is required by "
msgstr ", gereksinim duyan"

msgid ", is excluded"
msgstr ", dışlandı"

msgid "Packages excluded"
msgstr "Dışlanan paketler"

msgid "Packages to be installed"
msgstr "Kurulacak paketler"

msgid "Packages to be upgraded"
msgstr "Güncellenecek paketler"

msgid "Packages to be reinstalled"
msgstr "Packages to be reinstalled"

msgid "Packages to be removed"
msgstr "Kaldırılacak paketler"

msgid "Package"
msgstr "Paket"

#, c-format
msgid "%d upgraded, "
msgid_plural "%d upgraded, "
msgstr[0] "%d güncellenmiş,"
msgstr[1] "%d yükseltilmiş,"

#, c-format
msgid "%d reinstalled, "
msgid_plural "%d reinstalled, "
msgstr[0] "%dyeniden yüklenmiş,"
msgstr[1] "%d yeniden yüklenmiş,"

#, c-format
msgid "%d newly installed, "
msgid_plural "%d newly installed, "
msgstr[0] "%d yeni yüklenmiş,"
msgstr[1] "%d yenisi yüklenmiş,"

#, c-format
msgid "%d to remove "
msgid_plural "%d to remove "
msgstr[0] "%d "
msgstr[1] "%d 'i kaldır"

#, c-format
msgid "and %d not upgraded."
msgid_plural "and %d not upgraded."
msgstr[0] "ve %d yükseltilmemiş,"
msgstr[1] "ve %d yükseltilmemiş."

msgid "<span weight=\"bold\" size=\"large\">You don't have enough free space</span>"
msgstr "<span weight=\"bold\" size=\"large\">Yeterli yeriniz yok</span>"

#, c-format
msgid "Need to get %.0f%s/%.0f%s of archives.\n"
msgstr "Arşivlerden  %.0f%s/%.0f%s alınacak.\n"

#, c-format
msgid "Need to get %.0f%s of archives."
msgstr "Arşivlerden  %.0f%s alınacak."

#, c-format
msgid "After unpacking %.0f%s disk space will be freed."
msgstr "Paketlerin açılmasından sonra %.0f%s disk boşluğu kazanılacak."

#, c-format
msgid "After unpacking %.0f%s of additional disk space will be used."
msgstr "Paketlerin açılmasından sonra %.0f%s ilave disk boşluğu kullanılacak."

msgid "Up to Date"
msgstr "Güncel"

msgid "<span weight=\"bold\" size=\"large\">No updates available</span>"
msgstr "<span weight=\"bold\" size=\"large\">Şimdilik güncelleme yok :)</span>"

msgid "Downloading packages..."
msgstr "Paketler indiriliyor..."

msgid "Downloading..."
msgstr "İndiriliyor..."

#, c-format
msgid "Failed to download %s: %s"
msgstr "Failed to download %s: %s"

msgid "Removing packages..."
msgstr "Paketleri kaldırıyor..."

msgid "Installing packages..."
msgstr "Paketleri kuruyor..."

msgid "Uninstalling..."
msgstr "Kaldırılıyor..."

msgid "Installing..."
msgstr "Kuruyor..."

msgid "Upgrading..."
msgstr "Yükseltiyor..."

msgid "Failed to commit preferences"
msgstr "Ayarları uygulamada başarısızlık"

msgid "Pending changes. Click execute when ready."
msgstr "Değişiklikler beklemede. Hazır olduğunuzda Uygulayın."

msgid "Status"
msgstr "Durum"

msgid "Name"
msgstr "Ad"

msgid "Version"
msgstr "Sürüm"

msgid "Location"
msgstr "Konum"

msgid "Description"
msgstr "Açıklama"

msgid "Installed Size"
msgstr "Kurulum Büyüklüğü"

#, c-format
msgid "<b>Excluding %s due to dependency failure</b>"
msgstr "<b>Bağımlılık yetersizliği nedeniyle %s dışlanacak</b>"

msgid ""
"Missing dependencies may mean the software in this package will not function"
" correctly.  Do you want to continue without the required packages?"
msgstr "Kayıp bağımlılıklar dolayısıyla paketteki yazılım beklendiği gibi çalışmayabilir. Eksikliklere rağmen devam edilsin mi?"

msgid ": Depends: "
msgstr ":Bağımlılık:"

msgid "Import"
msgstr "Import"

msgid "No key found"
msgstr "No key found"

msgid "ChangeLogs"
msgstr "ChangeLogs"

msgid "No changelogs found."
msgstr "Değişim günlüğü bulunamadı."

msgid "Gslapt Package Manager"
msgstr "Gslapt Package Manager"

msgid "Install, remove and upgrade software packages"
msgstr "Install, remove and upgrade software packages"

msgid "Run Gslapt as root"
msgstr "Gslapt'u kök olarak çalıştırın"

msgid "Authentication is required to run Gslapt"
msgstr "Gslapt'ı çalıştırmak için kimlik doğrulaması gerekiyor"

msgid "Base"
msgstr "Base"

msgid "Applications"
msgstr "Applications"

msgid "Development Tools"
msgstr "Development Tools"

msgid "GNU Emacs"
msgstr "GNU Emacs"

msgid "Documentation"
msgstr "Documentation"

msgid "GNOME"
msgstr "GNOME"

msgid "Linux kernel"
msgstr "Linux kernel"

msgid "KDE"
msgstr "KDE"

msgid "KDE Internationalization"
msgstr "KDE Internationalization"

msgid "Libraries"
msgstr "Libraries"

msgid "Networking"
msgstr "Networking"

msgid "teTeX"
msgstr "teTeX"

msgid "TCL"
msgstr "TCL"

msgid "X Window System"
msgstr "X Window System"

msgid "X Applications"
msgstr "X Applications"

msgid "BSD Console games"
msgstr "BSD Console games"

msgid "Patches"
msgstr "Patches"

msgid "GSB Base"
msgstr "GSB Base"

msgid "GSB Accessibility"
msgstr "GSB Accessibility"

msgid "GSB Administration"
msgstr "GSB Administration"

msgid "GSB Applications"
msgstr "GSB Applications"

msgid "GSB Compiz Fusion"
msgstr "GSB Compiz Fusion"

msgid "GSB Development"
msgstr "GSB Development"

msgid "GSB Office"
msgstr "GSB Office"

msgid "GSB Libraries"
msgstr "GSB Libraries"

msgid "GSB Mono"
msgstr "GSB Mono"

msgid "GSB Networking"
msgstr "GSB Networking"

msgid "GSB OpenOffice.org"
msgstr "GSB OpenOffice.org"

msgid "GSB Themes"
msgstr "GSB Themes"

msgid "Games"
msgstr "Games"

msgid "Localization"
msgstr "Localization"

msgid ""
"gslapt is a GTK+ frontend to slapt-get, an APT like package system for "
"Slackware"
msgstr "gslapt, Slackware'de APT-benzeri paketleme sistemi olan slapt-get için bir GTK+ arabirimi sağlar"

msgid "working"
msgstr "çalışıyor"

msgid "total progress"
msgstr "toplam ilerleme"

msgid "Gslapt"
msgstr "Gslapt"

msgid "_File"
msgstr "_Dosya"

msgid "Update local package cache"
msgstr "Yerel paket zulasını güncelle"

msgid "Update"
msgstr "Güncelle"

msgid "Mark all possible upgrades"
msgstr "Tüm olası yükseltmeleri işaretle"

msgid "Mark All Upgrades"
msgstr "Tüm Yükseltmeleri İşaretle"

msgid "Mark all obsolete packages"
msgstr "Tüm terkedilmiş paketleri göster"

msgid "Mark Obsolete"
msgstr "Terkedilmiş olarak işaretle"

msgid "Execute all scheduled actions in current transaction"
msgstr "Şimdiki işlemde tüm planlanan eylemleri uygular"

msgid "Execute"
msgstr "Uygula"

msgid "_Edit"
msgstr "Dü_zen"

msgid "Unmark All"
msgstr "Tüm Seçimleri İptal et"

msgid "Preferences"
msgstr "Ayarlar"

msgid "_View"
msgstr "_Göster"

msgid "View all packages"
msgstr "Tüm paketleri göster"

msgid "All"
msgstr "Tümü"

msgid "View available packages from current sources"
msgstr "Halihazırdaki kaynaklarda müsait paketleri göster"

msgid "Available"
msgstr "Müsait"

msgid "View installed packages"
msgstr "Kurulmuş paketleri göster"

msgid "View Marked Packages"
msgstr "İşaretli Paketleri Göster"

msgid "Marked"
msgstr "İşaretli"

msgid "View Upgradeable Packages"
msgstr "View Upgradeable Packages"

msgid "Upgradeable"
msgstr "Upgradeable"

msgid "P_ackage"
msgstr "_Paket"

msgid "Unmark"
msgstr "İşaretini kaldır"

msgid "Install"
msgstr "Kur"

msgid "Downgrade"
msgstr "Alçalt"

msgid "Remove"
msgstr "Kaldır"

msgid "_Help"
msgstr "_Yardım"

msgid "Icon Legend"
msgstr "Simge Göstergesi"

msgid "View ChangeLogs"
msgstr "View ChangeLogs"

msgid "About"
msgstr "Hakkında"

msgid "Search"
msgstr "Ara"

msgid "<b>Name:</b>"
msgstr "<b>Adı:</b>"

msgid "No package is selected."
msgstr "Hiçbir paket seçilmedi."

msgid "<b>Status:</b>"
msgstr "<b>Durum:</b>"

msgid "<b>Description:</b>"
msgstr "<b>Açıklama:</b>"

msgid "<b>Version:</b>"
msgstr "<b>Sürüm:</b>"

msgid "<b>Source:</b>"
msgstr "<b>Kaynak:</b>"

msgid "<b>Priority:</b>"
msgstr "<b>Priority:</b>"

msgid "<b>Location:</b>"
msgstr "<b>Konum:</b>"

msgid "<b>Installed Version</b>"
msgstr "<b>Kurulu Sürüm</b>"

msgid "Version:"
msgstr "Sürüm:"

msgid "Installed Size:"
msgstr "Kurulum Büyüklüğü:"

msgid "<b>Latest Available Version</b>"
msgstr "<b>Uygun enson Sürüm</b>"

msgid "Size:"
msgstr "Büyüklük:"

msgid "Source:"
msgstr "Kaynak:"

msgid "Common"
msgstr "Ortak"

msgid "Dependencies"
msgstr "Bağımlılıklar"

msgid "Changelog"
msgstr "Changelog ( Değişiklikler )"

msgid "Files"
msgstr "Files"

msgid ""
"<b>The following icons are used to indicate the current status of a "
"package:</b>"
msgstr "<b>Paketin şimdiki durumunu belirtmek için aşağıdaki simgeler kullanılacak:</b>"

msgid "Repositories changed"
msgstr "Depo içeriği değişti"

msgid "<span size=\"large\" weight=\"bold\">Repositories changed</span>"
msgstr "<span size=\"large\" weight=\"bold\">Depo içeriği değişti</span>"

msgid ""
"You need to reload the package list from the sources you have listed.  Do "
"you want to do this now?"
msgstr "Listelediğiniz kaynaklardaki paket listelerini yeniden yüklemeniz gerekiyor. Bunu şimdi yapmak ister misiniz?"

msgid "Source failed"
msgstr "Source failed"

msgid ""
"This package source failed or is not available.  Do you want to disable this"
" source and continue anyway?"
msgstr "Bu paketin kaynağı yok veya bulunamıyor.  Ne olursa olsun,-işleme devam edilsin mi?"

msgid "Default"
msgstr "Default"

msgid "Official"
msgstr "Official"

msgid "Preferred"
msgstr "Preferred"

msgid "Custom"
msgstr "Custom"

msgid "Package Source"
msgstr "Package Source"

msgid "Summary"
msgstr "Özet"

msgid "<span weight=\"bold\" size=\"large\">Apply the following changes?</span>"
msgstr "<span weight=\"bold\" size=\"large\">Değişiklikler uygulansın mı?</span>"

msgid "This is a summary of the changes about to be performed."
msgstr "Gerçekleştirilen değişiklikler hakkında özet bilgi."

msgid "Download packages only"
msgstr "Paketleri sadece indir"

msgid "<b>Package and data storage</b>"
msgstr "<b>Paket ve veri deposu</b>"

msgid "_Delete Cached Package Files"
msgstr "Zuladaki Paket Dosyalarını _Sil"

msgid "Working Directory"
msgstr "Çalışma Dizini"

msgid "<b>Exclude List</b>"
msgstr "<b>Dışlanan Liste</b>"

msgid "<span weight=\"bold\">Exclude Expression:</span>"
msgstr "<span weight=\"bold\">Dışlanan İfade:</span>"

msgid "Excludes"
msgstr "Dışlananlar"

msgid "<b>Package Sources</b>"
msgstr "<b>Paket Kaynakları</b>"

msgid "Sources"
msgstr "Kaynaklar"

msgid "<b>GPG Keys</b>"
msgstr "<b>GPG Keys</b>"

msgid "Verification"
msgstr "Verification"
